# Progetto di gestione To-Do List - Django & React


### Descrizione del Progetto
Lo scopo di questo esercizio è sviluppare una robusta applicazione di gestione delle attività (To-Do List) utilizzando Django per il backend e React per il frontend. Questo progetto include funzionalità avanzate di gestione e interazione con le attività.

## Requisiti Tecnici
**Per sviluppatori Django:** Sviluppare un backend con Django che gestisca un elenco di attività. Utilizzare il sistema di modelli di Django per memorizzare le attività. Implementare le seguenti funzionalità:

- API GET /tasks per restituire l'elenco delle attività con paginazione.
- API POST /tasks per aggiungere una nuova attività con validazione dei dati.
- API PUT /tasks/:id per contrassegnare un'attività come completata e gestire la data di completamento.
- API DELETE /tasks/:id per rimuovere un'attività.

**Per sviluppatori React:** Creare il frontend utilizzando React per gestire l'interfaccia utente e le interazioni con l'utente. Implementare le seguenti funzionalità avanzate:

- Visualizzazione dell'elenco delle attività con ordinamento per data di creazione o stato.
- Implementazione di un form interattivo per l'aggiunta di nuove attività con validazione dei campi.
- Possibilità di modificare il testo delle attività direttamente nella lista.
- Aggiunta di un filtro per visualizzare solo le attività completate o non completate.
- Utilizzare LocalStorage per memorizzare le attività localmente nel browser con gestione delle modifiche.

## Utilizzo di Git e GitLab
Durante lo sviluppo del progetto, ogni sviluppatore dovrà:

- Inizializzare un repository Git locale.
- Creare un repository remoto su GitLab.
- Creare almeno cinque issues su GitLab per ciascun sviluppatore, assegnandole a sé stessi:
  - Per gli sviluppatori Django: implementazione delle API avanzate, gestione della validazione dei dati, gestione avanzata delle attività.
  - Per gli sviluppatori React: creazione dell'interfaccia utente avanzata, gestione delle interazioni utente, integrazione avanzata con LocalStorage.
- Fare commit regolari con messaggi significativi, facendo riferimento alle issues.
- Utilizzare tag per identificare versioni significative del progetto. Creare un tag v1.0 dopo aver completato tutte le funzionalità di base e averle testate.