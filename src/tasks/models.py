from django.db import models
from rest_framework import serializers
from django.utils import timezone

class Task(models.Model):
    titolo = models.CharField(max_length=255)
    descrizione = models.TextField()
    completata = models.BooleanField(default=False)
    data_completamento = models.DateTimeField(null=True, blank=True)

class TaskListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = [
            'id',
            'titolo',
            'descrizione',
            'completata',
            'data_completamento'
        ]

class TaskCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = [
            'id',
            'titolo',
            'descrizione'
        ]
        read_only_fields = [
            'id'
        ]

class TaskUpdateSerializer(serializers.ModelSerializer):
    def update(self, instance, validated_data):
        super().update(instance, validated_data | {
            'data_completamento': timezone.now()
        })
        return instance

    class Meta:
        model = Task
        fields = [
            'id',
            'completata',
            'data_completamento'
        ]
        read_only_fields = [
            'id',
            'data_completamento'
        ]

