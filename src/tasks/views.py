from django.urls import path
from rest_framework import viewsets
from .models import (
    Task,
    TaskListSerializer,
    TaskCreateSerializer,
    TaskUpdateSerializer
)

class TaskView(viewsets.ModelViewSet):
    queryset = Task.objects.all()

    def get_serializer_class(self):
        return globals()[f'Task{ self.action.capitalize() }Serializer']
 
urlpatterns = [
    path('', TaskView.as_view({
        'get': 'list',
        'post': 'create'
    })),
    path('<int:pk>/', TaskView.as_view({
        'put': 'update',
        'delete': 'destroy'
    }))
]

